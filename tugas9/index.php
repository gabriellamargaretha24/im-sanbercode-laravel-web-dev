<?php

require('animal.php');
require('frog.php');
require('ape.php');

$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Cold Blooded : $sheep->cold_blooded <br>";

echo "<br>";

$frog = new Frog("buduk");

echo "Name : $frog->name <br>";
echo "Legs : $frog->legs <br>";
echo "Cold Blooded : $frog->cold_blooded <br>";
$frog->jump();

echo "<br><br>";

$sungokong = new Ape("kera sakti");

echo "Name : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Cold Blooded : $sungokong->cold_blooded <br>";
$sungokong->yell();

echo "<br><br>";
