<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman Utama</title>
  </head>
  <body>
    <h1>Nekoyama-san Community</h1>
    <h2>Anime & Cosplay Community</h2>
    <p>
      Komunitas anime dan cosplay Indonesia yang menyenangkan dan bermanfaat.
      <br />
      Dengan beranggotakan lebih dari 500 member dari seluruh Indonesia.
    </p>

    <h2>Manfaat bergabung Nekoyama-san Community:</h2>
    <ul>
      <li>Menambah teman baru</li>
      <li>
        Dapat menambah wawasan tentang dunia anime, cosplay, dan japanese
        culture
      </li>
      <li>Tempat penyaluran hobi, pengalaman, dan lain-lain</li>
    </ul>

    <h2>Bagaimana cara bergabung:</h2>
    <ol>
      <li>Klik halaman ini</li>
      <li>Isi halaman ini <a href="/biodata">Klik di sini</a></li>
      <li>Selesai dan selamat bergabung Nekoyama-san Community</li>
    </ol>
  </body>
</html>
