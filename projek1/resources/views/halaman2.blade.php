<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Halaman Kedua</title>
  </head>
  <body>
    <h1>Ayo Buat Akunnya!</h1>
    <form action="/kirim" method="post">
        @csrf
      <h2>Sign Up Form</h2>

      <p>Nama</p>
      <input type="text" name="nama"> <br />

      <p>Usia</p>
      <input type="number" /> <br />

      <p>Password</p>
      <input type="password" /> <br />
      
      <p>Jenis Kelamin</p>
      <input type="radio" name="Jenis Kelamin" /> Laki-laki <br />
      <input type="radio" name="Jenis Kelamin" /> Perempuan <br />

      <p>Bahasa</p>
      <select name="Bahasa">
        <option value=>Bahasa Indonesia</option>
        <option value=>English</option>
        <option value=>Japanese (日本語)</option>
        <option value=>Other</option>
      </select> <br />

      <p>Hobi</p>
      <input type="checkbox" name="hobi" /> Penonton anime <br />
      <input type="checkbox" name="hobi" /> Cosplayer <br />
      <input type="checkbox" name="hobi" /> Other <br /> <br>

      <label>Quotes</label> <br />
      <textarea cols="20" rows="20"></textarea> <br />

      <input type="submit" value="Kirim" />
    </form>
  </body>
</html>
