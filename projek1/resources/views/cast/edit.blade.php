@extends('layout.master')

@section('table')
 Edit Data Pemain Film Baru
@endsection

@section('content')

<form action= "/cast/{{$cast->id}}" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama</label>
      <input type="text" name="nama" value="{{$cast->nama}} class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" value="{{$cast->umur}}" class="form-control" id="exampleInputPassword1">
    </div>
    @error('umur')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <div class="form-group">
      <label>Bio</label>
      <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->nama}}</textarea>
    </div>
    @error('bio')
        <div class="alert alert-danger">{{$message}}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection