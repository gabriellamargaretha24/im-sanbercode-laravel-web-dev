@extends('layout.master')

@section('table')
 Detail Data Pemain Film Baru
@endsection

@section('content')


<h1>{{$cast->nama}}</h1>
<p>{{$cast->umur}}</p>
<p>{{$cast->bio}}</p>

<a href="/cast" class="btn btn-secondary btm sm">Kembali</a>

@endsection