<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', [HomeController::class, 'utama']);

Route::get('/biodata', [AuthController::class, 'biodata']);

Route::post('/kirim', [AuthController::class, 'kirim']);

Route::get('/table', function () {
    return view('table');
});

Route::get('/datatable', function () {
    return view('datatable');
});

//CRUD Cast

//Create
//Form Tambah Cast
Route::get('/cast/create', [CastController::class, 'create']);
//Kirim + tambah data ke database
Route::post('/cast', [CastController::class, 'store']);

//Read
//Tampil semua data
Route::get('/cast', [CastController::class, 'index']);

//Detail
Route::get('/cast{cast_id}', [CastController::class, 'show']);

//Update
//Form Update
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//Update berdasarkan id
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete
Route::delete('/cast{cast_id}', [CastController::class, 'destroy']);
